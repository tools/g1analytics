# Ğ1 Analytics
This is a collection of tools for Ğ1 crypto currency transactions analysis.  
Author : François Libre  

## Commentaires
Word cloud of Ğ1 transaction comments.   
The scripts require a duniter node (V1) running on host, and a scheduled job to copy duniter txs.db files to DB_PATH 

Copy commentaires/www/index.html into your html root directory (ex: /var/www/html/commentaires)  
word cloud generated data will be uploaded in this html directory.   

Usage : 
Configure directory PATHS in update-wordcloud.py and update-wordcloud-perso.py 

For global blockchain comments :  
$ python update-wordcloud.py  

For personalized blockchain comments :  
Add public keys from which you wish to anlayze comments in commentaires/perso_pubkeys.txt  
$ python update-wordcloud-perso.py 

## Transactions
Requires a duniter node (V1) running on host, and to run a daily update python script.
Analysis of G1 transactions : pages/01_transactions.py and g1analytics.py
Consolidated data load and aggregation (to run daily) : transactions/update_transactions_data.py


