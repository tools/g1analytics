import sqlite3
import pandas as pd
import os
from datetime import timedelta
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta

from wordcloud import WordCloud

from stop_words import get_stop_words

import numpy as np
from PIL import Image

# This script requires a duniter node running on host, and a scheduled job to copy duniter txs.db files to DB_PATH

added_stop_words = ['Merci', 'Gracias', 'Bienvenue', 'Bienvenu', 'Bienvenida', 'June', 'G1', 'Juna', 'Junas', 'Bienvenido']
stop_words = get_stop_words('french') + get_stop_words('spanish') + added_stop_words

DB_PATH = '/home/francois/coding/g1/duniter_db_test/'
DB_FILE_TXS = os.path.join(DB_PATH, 'txs.db')
DB_FILE_DUNITER = os.path.join(DB_PATH, 'duniter.db')

WWW_PATH = '/var/www/html/gexplore/commentaires/'

connection_txs = sqlite3.connect(DB_FILE_TXS)

df_com = pd.read_sql_query("SELECT time,comment from txs", connection_txs, parse_dates=['time'])
date_6_months_before_today = (pd.to_datetime(pd.to_datetime('now', utc=True)) - relativedelta(months=6)).tz_localize(None)

comments_text = df_com.query("time >= @date_6_months_before_today")['comment'].str.cat(sep=' ')

# Standard wordcloud :
#wordcloud = WordCloud(width=800, height=1200, background_color ='black', stopwords=stop_words).generate(comments_text)

# Neat colors with white background
wordcloud = WordCloud(width=800, height=1200, background_color ='white', colormap='brg_r', stopwords=stop_words).generate(comments_text)

wordcloud.to_file(os.path.join(WWW_PATH, "gwordcloud.png"))
wordcloud.to_file(os.path.join(WWW_PATH, "archive", "gwordcloud_"\
                               + str(datetime.now().strftime("%Y%m%d"))\
                               + "_since_" + date_6_months_before_today.strftime('%Y%m%d')\
                               + ".png"))
                               
with open(os.path.join(WWW_PATH, 'update.txt'), 'w') as f:
    f.write(f"Comments from {date_6_months_before_today.strftime('%d/%m/%Y')} to {str(datetime.now().strftime('%d/%m/%Y'))} \n" + "\nWords not displayed : common french + spanish + " + str(added_stop_words))
