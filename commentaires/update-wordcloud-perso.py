import sqlite3
import pandas as pd
import os
from datetime import timedelta
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta

from wordcloud import WordCloud

from stop_words import get_stop_words

from itertools import chain
import ast

import numpy as np
from PIL import Image

# This script requires a duniter node running on host, and a scheduled job to copy duniter txs.db files to DB_PATH

added_stop_words = ['Merci', 'Gracias', 'Bienvenue', 'Bienvenu', 'Bienvenida', 'June', 'G1', 'Juna', 'Junas', 'Bienvenido']
stop_words = get_stop_words('french') + get_stop_words('spanish') + added_stop_words

DB_PATH = '/home/francois/coding/g1/duniter_db_test/'
DB_FILE_TXS = os.path.join(DB_PATH, 'txs.db')
DB_FILE_DUNITER = os.path.join(DB_PATH, 'duniter.db')

SCRIPT_PATH = '/home/francois/coding/g1/g1analytics/'
WWW_PATH = '/var/www/html/gexplore/commentaires/'

PERSO_PATH = os.path.join(SCRIPT_PATH, 'commentaires', 'perso_pubkeys.txt')

connection_txs = sqlite3.connect(DB_FILE_TXS)

df_txs = pd.read_sql_query("SELECT time,comment,issuers,recipients from txs", connection_txs, parse_dates=['time'])

PERSONALIZED_KEY_LIST = []
with open(PERSO_PATH) as file:
    for line in file:
        PERSONALIZED_KEY_LIST.append(line.rstrip())
        
for pkey in PERSONALIZED_KEY_LIST:
    print(f'Generating data for {pkey}')
    first_level_key_list = list(
        dict.fromkeys(
            list(chain.from_iterable(
                 [ast.literal_eval(k) for k in list(df_txs.query('recipients.str.contains(@pkey)')['issuers'])]
                 )
                 )\
        +\
             list(chain.from_iterable(
                 [ast.literal_eval(k) for k in list(df_txs.query('issuers.str.contains(@pkey)')['recipients'])]
                 )
                 )
        )
    )
    
    comments_text_perso_level1 = df_txs.loc[df_txs.issuers.apply(lambda x: True if any(i in x for i in first_level_key_list) else False)
              | df_txs.recipients.apply(lambda x: True if any(i in x for i in first_level_key_list) else False)
              ]['comment'].str.cat(sep=' ')
    
    comments_text_perso_level0 = df_txs.loc[df_txs.issuers.apply(lambda x: True if any(i in x for i in [pkey]) else False)
              | df_txs.recipients.apply(lambda x: True if any(i in x for i in [pkey]) else False)
              ]['comment'].str.cat(sep=' ')
    
    wordcloud_perso_level1 = WordCloud(width=800, height=1200, background_color ='white', colormap='brg_r', stopwords=stop_words + ['REMU']).generate(comments_text_perso_level1)
    wordcloud_perso_level0 = WordCloud(width=800, height=1200, background_color ='white', colormap='brg_r', stopwords=stop_words + ['REMU']).generate(comments_text_perso_level0)
    
    if not os.path.exists(os.path.join(WWW_PATH, pkey)):
        os.mkdir(os.path.join(WWW_PATH, pkey))
        os.mkdir(os.path.join(WWW_PATH, pkey, 'level0'))
        os.mkdir(os.path.join(WWW_PATH, pkey, 'level1'))
    
    nb_different_partners = len(first_level_key_list)
    date_maj = str(datetime.now().strftime("%Y%m%d"))
    
    with open(os.path.join(WWW_PATH, pkey, 'index.html'), 'w') as f:
        f.write(f'''<!DOCTYPE html>
<html>
<body>
<h1>Nuages de mots personnalis&eacute;s</h1>
Vous avez effectu&eacute; des transactions avec {nb_different_partners} tiers diff&eacute;rents
<p><a href="level0/gwordcloud_l0_'''+pkey+'''.png">Cliquez ici pour un nuage de mots uniquement de vos transactions</a></p>
<p><a href="level1/gwordcloud_l1_'''+pkey+f'''.png">Cliquez ici pour un nuage de mots avec vos transactions et celles de vos {nb_different_partners} partenaires</a></p>

Date de mise &agrave; jour : {date_maj}
</body>
</html>        
        ''')
        
    wordcloud_perso_level1.to_file(os.path.join(WWW_PATH, pkey, 'level1', 'gwordcloud_l1_'+pkey+'.png'))
    wordcloud_perso_level0.to_file(os.path.join(WWW_PATH, pkey, 'level0', 'gwordcloud_l0_'+pkey+'.png'))
    
print('End')
